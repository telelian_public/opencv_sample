from threading import Thread
from time import sleep
import logging 
import cv2 

logging.basicConfig(level=logging.DEBUG)

class Camera:
    def __init__(self, num=0, width=1920, height=1080, fps=30, camera_format='UYVY', args=None):
        self.num = num
        self.setCaps(num, width, height, fps, camera_format, args)
        logging.debug({
            'caps':self.__gst_cap
        })
        self._cap = cv2.VideoCapture(self.__gst_cap, cv2.CAP_GSTREAMER)

        
    def setCaps(self, num, width, height, fps, camera_format, args):
        if args:
            cam_args = args
        else:
            cam_args = ''
               
        self.__caps_filter = f'video/x-raw(memory:NVMM),width=(int){width},height=(int){height},format=(string){camera_format},framerate=(fraction){fps}/1'
        
        self.__gst_cap = f'nvv4l2camerasrc device=/dev/video{num} {cam_args} ! {self.__caps_filter} ! nvvidconv ! video/x-raw,format=(string)BGRx ! videoconvert ! video/x-raw,format=(string)BGR ! appsink sync=false name=appsink0'

    def loop(self):
        self._run = True
        logging.debug(f'loop {self.num} start') 
        if self._cap.isOpened():
            logging.debug(f'camera {self.num} opened : {self._cap.get(3)}, {self._cap.get(4)}')

        while self._run:
            ret, frame = self._cap.read()        
            if ret:
                isquit = self.render(frame)
                if isquit:
                    break
                

    def render(self, img):
        cv2.imshow(f'video{self.num}', img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            return True 
        else:
            return False

    def stop(self):
        self._run = False

if __name__ == '__main__':
    import signal
    def sig_handler(signum, frame):
        global cam, th
        logging.critical(
            {
                'title': 'signal',
                'signum': signum,
                'frame': frame
            }
        )
        
        cam.stop()
        th.join()
    
    for signame in {signal.SIGINT, signal.SIGTERM}:
        signal.signal(signame, sig_handler)

    cam = Camera(num=0)
    
    th = Thread(target=cam.loop)
    th.start()

    
    