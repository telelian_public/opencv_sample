from threading import Thread
from time import sleep
import logging 
import cv2 

logging.basicConfig(level=logging.DEBUG)

class Camera:
    def __init__(self, num=0, width=1920, height=1080, fps=30, camera_format='UYVY',file_loc=None, args=None):
        self.num = num
        self.setCaps(num, width, height, fps, camera_format, file_loc, args)
        logging.debug({
            'caps':self.__gst_cap
        })
        self._cap = cv2.VideoCapture(self.__gst_cap, cv2.CAP_GSTREAMER)

        # cascPath = "/usr/share/opencv4/haarcascades/haarcascade_frontalface_default.xml" 
        # cascPath = "/usr/share/opencv4/haarcascades/haarcascade_frontalface_alt2.xml"
        cascPath = "/usr/share/opencv4/lbpcascades/lbpcascade_frontalface_improved.xml"                                 
        # Create the cascade
        self.classifier = cv2.CascadeClassifier(cascPath)

    def setCaps(self, num, width, height, fps, camera_format, file_loc, args):
        if args:
            cam_args = args
        else:
            cam_args = ''
               
        self.__caps_filter = f'video/x-raw(memory:NVMM),width=(int){width},height=(int){height},format=(string){camera_format},framerate=(fraction){fps}/1'
        self._file_tee = f''
        
        if file_loc:
            self._file_tee = f'! tee name=t  t. ! queue ! nvvidconv ! video/x-raw(memory:NVMM), format=(string)I420 ! queue ! nvv4l2h265enc ! h265parse ! matroskamux name=mux streamable=true ! filesink location={file_loc} sync=false  t. ! queue '
        
        self.__gst_cap = f'nvv4l2camerasrc device=/dev/video{num} {cam_args} ! {self.__caps_filter} {self._file_tee} ! nvvidconv ! video/x-raw,format=(string)BGRx ! videoconvert ! video/x-raw,format=(string)BGR ! appsink sync=false name=appsink0'

    def loop(self):
        self._run = True
        logging.debug(f'loop {self.num} start') 
        if self._cap.isOpened():
            logging.debug(f'camera {self.num} opened : {self._cap.get(3)}, {self._cap.get(4)}')

        while self._run:
            ret, frame = self._cap.read()
            
            if ret:
                self.img_proc(frame)
                isquit = self.render(frame)
                if isquit:
                    break;
                
    def img_proc(self, img):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # Detect faces in the image
        
        # use cpu 
        faces = self.classifier.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
        )
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)

    def render(self, img):
        cv2.imshow(f'video{self.num}', img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            return True 
        else:
            return False

    def stop(self):
        self._run = False

if __name__ == '__main__':
    import signal
    def sig_handler(signum, frame):
        global cam, th
        logging.critical(
            {
                'title': 'signal',
                'signum': signum,
                'frame': frame
            }
        )
        
        cam.stop()
        th.join()
    
    for signame in {signal.SIGINT, signal.SIGTERM}:
        signal.signal(signame, sig_handler)

    cam = Camera(num=0, file_loc='./test.mkv')
    
    th = Thread(target=cam.loop)
    th.start()

    
    