# opencv_sample
## requirements
- nvidia-jetpack

## add docker group
```bash
sudo usermod -aG docker $USER
# re-login
```


## docker
### login
- 아래 링크 참조
    - https://devocean.sk.com/blog/techBoardDetail.do?ID=163773 

### pull
```bash
docker pull nvcr.io/nvidia/l4t-ml:r35.2.1-py3
```

### run
```bash
export DISPLAY=:0
xhost +local:docker
export OPENCV_SAMPLE_PATH=~/work/opencv_sample
export XAUTH=/tmp/.docker.xauth
docker run --runtime nvidia -it --rm --network host \
-e DISPLAY=$DISPLAY \
-v /tmp/.X11-unix/:/tmp/.X11-unix \
-v $XAUTH:$XAUTH \
-e XAUTHORITY=$XAUTH \
-v /tmp/camsock:/tmp/camsock \
--device /dev/video0 \
--device /dev/video1 \
--device /dev/video2 \
--device /dev/video3 \
--device /dev/capture-vi-channel0 \
--device /dev/capture-vi-channel1 \
--device /dev/capture-vi-channel2 \
--device /dev/capture-vi-channel3 \
-v $OPENCV_SAMPLE_PATH:/root/opencv_sample \
nvcr.io/nvidia/l4t-ml:r35.2.1-py3 
```


## in container
```bash
cd ~/opencv_sample
# face detection with cpu
python3 camera_mkv_cpu.py
# face detection with gpu
python3 camera_mkv_gpu.py
```

